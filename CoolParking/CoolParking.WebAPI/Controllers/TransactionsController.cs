using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoolParking.WebAPI.Services;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private TransactionsService transactionsService;
        public TransactionsController() {
            transactionsService = new TransactionsService();
        }

        
        [Route("last")]
        public ActionResult<TransactionInfo[]> Get()
        {
             return Ok(transactionsService.GetLast());
        }

        [Route("all")]
        public ActionResult<string> Get(string id){
            try{
                 return Ok(transactionsService.GetAll());    
            }
            catch{
                return StatusCode(404);
            }
            
        }   
    }
}
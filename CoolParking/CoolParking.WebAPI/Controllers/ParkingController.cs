using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        
        private ParkingServ parcingService;
        public ParkingController() {
            parcingService = new ParkingServ();
        }

        [Route("balance")]
        public ActionResult<string> Balance() {
            return Ok(parcingService.GetBalance());
        }

        [Route("capacity")]
        public ActionResult<int> Capacity() {
             return Ok(parcingService.GetCapacity());
        }

        [Route("freePlaces")]
        public ActionResult<int> FreePlaces() {
             return Ok(parcingService.GetFreePlaces());
        }
    }
}
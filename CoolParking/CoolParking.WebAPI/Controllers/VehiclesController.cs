using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using CoolParking.WebAPI.Services;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private VehiclesService vehiclesService;
        public VehiclesController() {
            vehiclesService = new VehiclesService();
        }

        
        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> Get(){
             return Ok(vehiclesService.Get());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id){
            Regex regex = new Regex(@"\S{2}-\d{4}-\S{2}");
            if (regex.Matches(id).Count <= 0){
                return StatusCode(400);
            }
            if(vehiclesService.GetId(id) == null){
                return StatusCode(404);
            }
            
            return Ok(vehiclesService.GetId(id));    
        } 

        [HttpPost]
        public void Post([FromBody] string value)
        { 
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult Delete(string id) {  
            Regex regex = new Regex(@"\S{2}-\d{4}-\S{2}");
            if (regex.Matches(id).Count <= 0){
                return StatusCode(400);
            }
            if(vehiclesService.GetId(id) == null){
                return StatusCode(404);
            }                      
            vehiclesService.DeleteVehicle(id);
            return StatusCode(204);     
        }
    }
}
using System.Collections.Generic;
using CoolParking.WebAPI;
using CoolParking.BL.Services;

using CoolParking.BL.Models;
using System.Collections.ObjectModel;


namespace CoolParking.WebAPI.Services
{
    public class TransactionsService
    {
        private ParkingService ps;
       
        public TransactionsService() {
             ps = new ParkingService();
        }

        public TransactionInfo[] GetLast() {
            return  ps.GetLastParkingTransactions();
        }

        public string GetAll() {
            return  ps.ReadFromLog(); 
        }

        public void DeleteVehicle(string vehicleId) {
            ps.RemoveVehicle(vehicleId);
        }
    }
}
using System.Collections.Generic;
using CoolParking.WebAPI;
using CoolParking.BL.Services;

using CoolParking.BL.Models;
using System.Collections.ObjectModel;


namespace CoolParking.WebAPI.Services
{
    public class VehiclesService
    {
        private ParkingService ps;
       
        public VehiclesService() {
             ps = new ParkingService();
        }

        public ReadOnlyCollection<Vehicle> Get() {
            return  ps.GetVehicles();
        }

        public Vehicle GetId(string vehicleId) {
            bool isEmpty = true;
            foreach (var item in ps.GetVehicles()) {
                if (item.Id == vehicleId){
                    isEmpty = false;
                }
            }
            if (isEmpty == true || Parking.collectionVehicle[vehicleId].Balance < 0){
                return null;
            }

            return  Parking.collectionVehicle[vehicleId]; 
        }

        public void DeleteVehicle(string vehicleId) {
            ps.RemoveVehicle(vehicleId);
        }
    }
}
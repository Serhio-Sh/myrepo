using System.Collections.Generic;
using CoolParking.WebAPI;
using CoolParking.BL.Services;

using CoolParking.BL.Models;


namespace CoolParking.WebAPI.Services
{
    public class ParkingServ
    {
        private ParkingService ps;
       
        public ParkingServ() {
            ps = new ParkingService();
        }

        public decimal GetBalance() {
            return  ps.GetBalance();
        }

        public int GetCapacity() {
            return  ps.GetCapacity();
        }
        
        public int GetFreePlaces() {
            return  ps.GetFreePlaces();
        }
    }
}
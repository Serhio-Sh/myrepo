using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace ParkingPr
{
    class Program
    {
        static void Main(string[] args)
        {
            TimerService TimerServiceForPay = new TimerService();

            TimerService TimerServiceForLog = new TimerService();

            string writePath = @"D:\ПРаГраМІст\Satisfaction\Parking\Transactions.log";

            LogService logService = new LogService(writePath);

            ParkingService parkingService = new ParkingService(TimerServiceForPay, TimerServiceForLog, logService);

            bool alive = true;

            while (alive)
            {
                ConsoleColor color = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGreen; // выводим список команд зеленым цветом
                Console.WriteLine("1. Вивести на екран поточний баланс Паркiнгу. \n2. Вивести на екран суму зароблених коштiв за поточний перiод (до запису у лог)  \n3. Вивести на екран кiлькiсть вiльних мiсць на паркуваннi(вільно X з Y)");
                Console.WriteLine("4. Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод (до запису у лог) \n5. Вивести на екран iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log) \n6. Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу.");
                Console.WriteLine("7. Поставити Транспортний засiб на Паркiнг. \n8. Забрати Транспортний засiб з Паркiнгу.\n9. Поповнити баланс конкретного Тр. засобу..");
                Console.WriteLine("Введiть циферку:");
                Console.ForegroundColor = color;
                try
                {
                    int command = Convert.ToInt32(Console.ReadLine());

                    switch (command)
                    {
                        case 1:
                            Console.WriteLine(parkingService.GetBalance());
                            break;
                        case 2:
                            Console.WriteLine(LastSumTrsactions(parkingService));
                            break;
                        case 3:
                            Console.WriteLine(parkingService.GetFreePlaces());
                            break;
                        case 4:
                            LastParkingTransactions(parkingService);
                            break;
                        case 5:
                            Console.WriteLine(parkingService.ReadFromLog());
                            break;
                        case 6:
                            foreach (var vehicle in parkingService.GetVehicles())
                            {
                                Console.WriteLine(vehicle.Id);
                            }
                            break;
                        case 7:
                            parkingService.AddVehicle(ChoseVehicle());
                            break;
                        case 8:
                            Console.WriteLine("Введiть ID:");
                            parkingService.RemoveVehicle(Console.ReadLine());
                            break;
                        case 9:
                            var (Id, sum) = Input();
                            parkingService.TopUpVehicle(Id, sum);
                            break;
                        default: throw new ArgumentException("Недопустимий код операцiї");
                    }
                }
                catch (Exception ex)
                {
                    color = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = color;
                }
            }
        }
        private static Vehicle ChoseVehicle()
        {
            Console.WriteLine("Введiть ID(формату DV-2345-KJ):");
            string Id = Console.ReadLine();

            Console.WriteLine("Виберiть тип вашої кобилки: \n 1.Легкова\n 2.Вантажна \n 3.Автобус\n 4.Мотоцикл");
            int order = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введiть баланс:");
            decimal balanse = Convert.ToDecimal(Console.ReadLine());

            Vehicle Chose() => order switch
            {
                1 => new Vehicle(Id, VehicleType.PassengerCar, balanse),
                2 => new Vehicle(Id, VehicleType.Truck, balanse),
                3 => new Vehicle(Id, VehicleType.Bus, balanse),
                4 => new Vehicle(Id, VehicleType.Motorcycle, balanse),
                _ => throw new ArgumentException("Недопустимий код операції")
            };

            return Chose();
        }

        public static (string id, decimal sum) Input()
        {
            Console.WriteLine("Введiть ID(формату DV-2345-KJ):");
            string Id = Console.ReadLine();

            Console.WriteLine("Введiть баланс:");
            decimal balanse = Convert.ToDecimal(Console.ReadLine());

            return (Id, balanse);
        }
        public static void LastParkingTransactions(ParkingService service)
        {
            foreach (var transaction in service.GetLastParkingTransactions())
            {
                Console.WriteLine("Час: " + Convert.ToString(transaction.Time) + " | Iдентифiкатор: " + transaction.Id + " | Сума списання: " + transaction.Sum);
            }
        }

        public static decimal LastSumTrsactions(ParkingService service)
        {
            decimal sum = 0;

            foreach (var transaction in service.GetLastParkingTransactions())
            {
                sum += transaction.Sum;
            }

            return sum;
        }

    }
}

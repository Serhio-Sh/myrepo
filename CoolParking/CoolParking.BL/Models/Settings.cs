// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal PrimaryBalanceOfParking { get; set; } = 0;
        public static int CapacityParking { get; set; } = 10;
        public static int PeriodWithdraw { get; set; } = 5;
        public static int PeriodRecord { get; set; } = 60;

        public static Dictionary<VehicleType, decimal> Tariffs { get; set; } = new Dictionary<VehicleType, decimal>
        {
            [VehicleType.PassengerCar] = 2m,
            [VehicleType.Truck] = 5m,
            [VehicleType.Bus] = 3.5m,
            [VehicleType.Motorcycle] = 1m
        };

        public static decimal Сoefficient { get; set; } = 2.5m;
    }
}
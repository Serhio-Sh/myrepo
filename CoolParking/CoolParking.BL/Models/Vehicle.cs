// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string Id, VehicleType VehicleType, decimal balance)
        {
            Regex regex = new Regex(@"\S{2}-\d{4}-\S{2}");
            if (regex.Matches(Id).Count <= 0 || balance < 0)
            {
                throw new ArgumentException();
            }
            else
            {
                this.Id = Id;
                this.VehicleType = VehicleType;
                this.Balance = balance;
            }
 
        }

        public string Id { get; } = GenerateRandomRegistrationPlateNumber();
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string randId = "";

            for (int i = 0; i < 4; i++)
            {
                if (i == 2)
                {
                    randId += "-";

                    for (int j = 0; j < 4; j++)
                    {
                        randId += random.Next(0, 9);
                    }

                    randId += "-";
                }

                randId += (char)random.Next(65, 91);
            }

            return randId;
        }
    }
}
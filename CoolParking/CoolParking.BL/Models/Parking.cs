// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking 
    {
        public static decimal Balance { get; internal set; }
        public static Dictionary<string, Vehicle> collectionVehicle { get; set; } = new Dictionary<string, Vehicle>();

        private static Parking instance;

        private Parking()
        { }

        public static Parking getInstance()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }
    }
}
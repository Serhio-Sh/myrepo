// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

public class TimerService : ITimerService
{
    private static Timer Timer;
    public double Interval { get; set; } 

    public event ElapsedEventHandler Elapsed;

    public void Dispose()
    {
        throw new System.NotImplementedException();
    }

    public void Start()
    {
        Timer = new Timer(Interval);
        Timer.Elapsed += Elapsed;
        Timer.AutoReset = true;
        Timer.Enabled = true;
    }

    public void Stop() => Timer.Stop();
}
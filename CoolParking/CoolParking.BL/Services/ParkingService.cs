// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
       //public delegate void ParkingServiceHandler(object sender, ElapsedEventArgs e);

        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            _withdrawTimer.Elapsed += TransferMoneyHandler;
            _logTimer.Elapsed += LogHandler;

            _withdrawTimer.Interval = Settings.PeriodWithdraw * 1000;
            _logTimer.Interval = Settings.PeriodRecord * 1000;

            _logTimer.Start();
            _withdrawTimer.Start();

            LogService = _logService;

            Parking.getInstance();
        }
        
        public ParkingService() {}
        
        private void TransferMoneyHandler(object sender, ElapsedEventArgs e)
        {
            decimal balance, tariff, removMoney;

            foreach (var vehicle in Parking.collectionVehicle.Values)
            {
                balance = vehicle.Balance;
                tariff = Settings.Tariffs[vehicle.VehicleType];

                if (balance < 0)
                {
                    removMoney = tariff * Settings.Сoefficient;
                    vehicle.Balance -= removMoney;
                }
                else if (balance > 0 && balance - tariff < 0)
                {
                    removMoney = balance + (tariff - balance) * Settings.Сoefficient;
                    Parking.Balance += balance;
                    vehicle.Balance -= removMoney;
                } 
                else
                {
                    removMoney = tariff;
                    vehicle.Balance -= removMoney;
                    Parking.Balance += removMoney;
                }

                TransIonInfoList.Add(new TransactionInfo() { Time = DateTime.Now, Id = vehicle.Id, Sum = removMoney });
            }          
        }

        private void LogHandler(object sender, ElapsedEventArgs e)
        {      
            foreach (var transaction in TransIonInfoList)
            {
                LogService.Write("Час: " + Convert.ToString(transaction.Time) + " | Iдентифікатор: " + transaction.Id + " | Сума списання: " + transaction.Sum);
            }

            TransIonInfoList = new List<TransactionInfo>();
        }

        public ILogService LogService { get; set; }

        public TransactionInfo[] TransIonInfo { get; set; }

        public List<TransactionInfo> TransIonInfoList { get; set; } = new List<TransactionInfo>();

        public decimal GetBalance() => Parking.Balance;

        public int GetCapacity() => Settings.CapacityParking - Parking.collectionVehicle.Count;       

        public int GetFreePlaces() => Settings.CapacityParking - Parking.collectionVehicle.Count;

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            List<Vehicle> LVehice = new List<Vehicle>();

            foreach (var item in Parking.collectionVehicle)
            {
                LVehice.Add(item.Value);
            }

            return new ReadOnlyCollection<Vehicle>(LVehice); 
        }

        public void AddVehicle(Vehicle vehicle)
        {
            foreach (var item in GetVehicles())
            {
                if (item.Id == vehicle.Id)
                {
                    throw new ArgumentException();
                }
            }
            if (Parking.collectionVehicle.Count == Settings.CapacityParking)
            {
                throw new InvalidOperationException();
            }
            else
            {
                Parking.collectionVehicle.Add(vehicle.Id, vehicle);
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            bool isEmpty = true;
            foreach (var item in GetVehicles())
            {
                if (item.Id == vehicleId)
                {
                    isEmpty = false;
                }
            }
            if (isEmpty == true || Parking.collectionVehicle[vehicleId].Balance < 0)
            {
                throw new ArgumentException();
            }
            else
            {
                Parking.collectionVehicle.Remove(vehicleId);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            bool empty = true;
            foreach (var item in GetVehicles())
            {
                if (item.Id == vehicleId)
                {
                    empty = false;
                }
            }
            if (empty == true || sum < 0)
            {
                throw new ArgumentException();
            }

            Parking.collectionVehicle[vehicleId].Balance += sum;
        }


        public void Dispose()
        {
            Parking.collectionVehicle = new Dictionary<string, Vehicle>();
            Parking.Balance = 0;
        }

        public TransactionInfo[] GetLastParkingTransactions() => TransIonInfoList.ToArray();

        public string ReadFromLog() => LogService.Read();
    }
}

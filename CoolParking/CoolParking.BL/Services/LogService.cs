// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string _logFilePath)
        {
            LogPath = _logFilePath;
        }


        public string LogPath { get; }

        public string Read()
        {
            string LogHistory = "";
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    LogHistory = sr.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                throw new InvalidOperationException();
            }

            return LogHistory;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(logInfo);
            }
        }
    }
}

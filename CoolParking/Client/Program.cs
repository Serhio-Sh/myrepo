using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        private const string APP_PATH = "https://localhost:44329";
        static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {

            bool alive = true;
            while (alive)
            {
                ConsoleColor color = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("1. Вивести на екран поточний баланс Паркiнгу. \n2. Вивести на екран кiлькiсть вiльних мiсць на паркуваннi(вільно X з Y)");
                Console.WriteLine("4. Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод (до запису у лог) \n5. Вивести на екран iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log) \n6. Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу.");
                Console.WriteLine("7. Поставити Транспортний засiб на Паркiнг. \n8. Забрати Транспортний засiб з Паркiнгу.\n9. Поповнити баланс конкретного Тр. засобу..");
                Console.WriteLine("Введiть циферку:");
                Console.ForegroundColor = color;

                try
                {
                    int command = Convert.ToInt32(Console.ReadLine());
                    switch (command)
                    {
                        case 1:
                            Console.WriteLine(GetBalance(client));
                            break;
                        case 2:
                            Console.WriteLine(GetCapacity(client));
                            break;
                        case 3:

                            break;
                        case 4:

                            break;
                        case 5:

                            break;
                        case 6:

                            break;
                        case 7:

                            break;
                        case 8:

                            break;
                        case 9:

                            break;
                        default: throw new ArgumentException("Недопустимий код операцiї");
                    }
                }
                catch (Exception ex)
                {
                    color = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = color;
                }
            }
        }

        private static string GetBalance(HttpClient client)
        {
            var response = client.GetAsync(APP_PATH + "/api/parking/balance").Result;
            return response.Content.ReadAsStringAsync().Result;

        }

        private static string GetCapacity(HttpClient client)
        {
            var response = client.GetAsync(APP_PATH + "/api/parking/capacity").Result;
            return response.Content.ReadAsStringAsync().Result;
        }        
    }
}
